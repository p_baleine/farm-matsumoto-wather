"use strict";

const Promise = require('bluebird');
const fs = require('fs');
const join = require('path').join;
const url = require('url');
const request = require('request');
const convert = require('color-convert');
const moment = require('moment');
const PNG = require('pngjs').PNG;

// http://m.map.c.yimg.jp/m?x=14281&y=1621&z=15&r=1&mode=weather-radar
// http://weather.map.c.yimg.jp/weather250?x=14281&y=1621&z=15&date=201607311155

// とりあえず、上記画像を縦横4分割したときの一番右下のエリアの平均レベルを求める

const weatherImgUrlBase = 'http://weather.map.c.yimg.jp/weather250?x=14281&y=1621&z=15&date=';

const levels = [
    "#ccffff",
    "#66ffff",
    "#00ccff",
    "#0099ff",
    "#3366ff",
    "#33ff00",
    "#33cc00",
    "#199900",
    "#ffff00",
    "#ffcc00",
    "#ff9900",
    "#ff5066",
    "#ff0000",
    "#b70014"
];

function nearest5minute(m) {
  let minutes = m.minutes();
  m.minutes(5 * Math.round(minutes / 5));
  return m.startOf('minutes');
}

function levelMatrix(png) {
  let matrix = [];
  for (let y = 0; y < png.height; y++) {
    let line = [];
    for (let x = 0; x < png.width; x++) {
      let idx = (png.width * y + x) << 2;
      let hex = convert.rgb.hex(png.data[idx], png.data[idx+1], png.data[idx+2]);
      line.push(levels.indexOf(`#${hex}`.toLowerCase()));
    }
    matrix.push(line);
  }
  return matrix;
}

function slice(twodarray, beginY, endY, beginX, endX) {
  return twodarray
    .slice(beginY, endY)
    .map((l) => l.slice(beginX, endX));
}

function flatten(nested) {
  return [].concat.apply([], nested);
}

function weatherImgUrl(m) {
  return `${weatherImgUrlBase}${nearest5minute(m).format('YYYYMMDDHHmm')}`;
}

function saveToPath(m, imgDir) {
  return join(imgDir, url.parse(weatherImgUrl(m)).path.replace('\/', ''));
}

function saveWeatherImg(m, imgDir) {
  return new Promise((resolve, reject) => {
    request(weatherImgUrl(m))
      .pipe(fs.createWriteStream(saveToPath(m, imgDir)))
      .on('close', resolve)
      .on('error', reject);
  });
}

function getAverageLevel(m, imgDir) {
  return new Promise((resolve, reject) => {
    let imgPath = saveToPath(m, imgDir);
    fs.createReadStream(imgPath)
      .pipe(new PNG({filterType: 4}))
      .on('parsed', function() {
        let matrix = levelMatrix(this);
        matrix = slice(matrix, this.height / 4 * 3, this.height,
                       this.width / 4 * 3, this.width);
        let flattened = flatten(matrix);
        resolve({
          level: flattened.reduce((a, b) => a + b, 0) / flattened.length,
          path: imgPath
        });
      })
      .on('error', reject);
  });
}

module.exports = function saveImageAndGetAverageLevel(m, imgDir) {
  return saveWeatherImg(m, imgDir).then(() => getAverageLevel(m, imgDir));
};
